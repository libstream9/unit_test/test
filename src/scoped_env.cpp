#include <stream9/test/scoped_env.hpp>

#include <cstdlib>
#include <ctime>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace test = stream9::test;

static std::string
random_string(int length)
{
    std::string result;

    std::srand(static_cast<unsigned>(std::time(nullptr)));

    for (auto i = 0; i < length; ++i) {
        result.push_back(static_cast<char>('A' + std::rand() % 26));
    }

    return result;
}

static std::string
random_env()
{
    std::string result;

    while (true) {
        result = random_string(10);
        if (::getenv(result.c_str()) == nullptr) {
            break;
        }
    }

    return result;
}

BOOST_AUTO_TEST_SUITE(scoped_env_)

    BOOST_AUTO_TEST_CASE(existing_env_)
    {
        auto const& name = random_env();

        ::setenv(name.c_str(), "foo", 1);
        BOOST_TEST(::getenv(name.c_str()) == "foo");
        {
            test::scoped_env e { name, "bar" };

            BOOST_TEST(::getenv(name.c_str()) == "bar");
        }
        BOOST_TEST(::getenv(name.c_str()) == "foo");
    }

    BOOST_AUTO_TEST_CASE(non_existing_env_)
    {
        auto const& name = random_env();

        BOOST_TEST(::getenv(name.c_str()) == nullptr);
        {
            test::scoped_env e { name, "bar" };

            BOOST_TEST(::getenv(name.c_str()) == "bar");
        }
        BOOST_TEST(::getenv(name.c_str()) == nullptr);
    }

BOOST_AUTO_TEST_SUITE_END() // scoped_env_

} // namespace testing
